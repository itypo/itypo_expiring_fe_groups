<?php 

if (!defined ('TYPO3_MODE')) die ('Access denied.'); 

t3lib_extMgm::addService($_EXTKEY,  'auth' /* sv type */,  'tx_itypoexpiringfegroups_sv1' /* sv key */,
	array(
		'title' 		=> 'Expiring FE Groups',
		'description' 	=> 'Filters out any expired FE groups.',
		'subtype' 		=> 'getGroupsFE',
		'available' 	=> true,
		'priority' 		=> 15,
		'quality' 		=> 15,
		'os' 			=> '',
		'exec' 			=> '',
		'classFile' 	=> t3lib_extMgm::extPath($_EXTKEY).'sv1/class.tx_itypoexpiringfegroups_sv1.php',
		'className' 	=> 'tx_itypoexpiringfegroups_sv1',
	)
);

?>