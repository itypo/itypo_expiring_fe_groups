<?php

if (!defined ('TYPO3_MODE')) die ('Access denied.');

$tempColumns = Array (
	'tx_itypoexpiringfegroups_groups' => Array (		
		'exclude' => 1,		
		'label' => 'LLL:EXT:'.$_EXTKEY.'/locallang_db.xml:fe_users.tx_itypoexpiringfegroups_groups',		
		'config' => Array (
			'type' => 'user',
			'size' => '5',
			'userFunc' => 'tx_itypoexpiringfegroups_selector->main',
		)
	),
);

t3lib_div::loadTCA('fe_users');
t3lib_extMgm::addTCAcolumns('fe_users',$tempColumns,1);
t3lib_extMgm::addToAllTCAtypes('fe_users','tx_itypoexpiringfegroups_groups','','after:usergroup');

// Add wizard icon to the content element wizard.
if (TYPO3_MODE == 'BE')	{
	require_once(t3lib_extMgm::extPath($_EXTKEY).'lib/class.tx_itypoexpiringfegroups_selector.php');
}

?>