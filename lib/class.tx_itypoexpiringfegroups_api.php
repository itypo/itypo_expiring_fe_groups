<?php

/***************************************************************
*  Copyright notice
*  
*  (c) 2013 Sander Leeuwesteijn | iTypo (info@itypo.nl)
*  Based on extension from: (c) 2004 Radu Cocieru (raducocieru@hotmail.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/** 
 * Library of some helper functions.
 *
 * @author	Sander Leeuwesteijn | iTypo <info@itypo.nl>
 * @author	Radu Cocieru <raducocieru@hotmail.com>
 */
class tx_itypoexpiringfegroups_api{
	function is_member_of(&$user, $group_id){
        $exp = $this->expires_in(&$user, $group_id);
		return ($exp && ($exp > 0));
	}
	
	function expires_in(&$user, $group_id){
		$userRecs = $this->fetch_groups($user);
		$userRecs = $userRecs[$group_id];
		if (is_array($userRecs)) {
			$t = 0;
			$now = time();
			foreach ($userRecs as $item){
				if ($item['startdate'] > 0 && $item['startdate'] > $now) continue;
				if ($item['enddate'] > 0 && $now > $item['enddate']) continue;
				$tt = $item['enddate']-$now;
				if( $tt > $t ) $t = $tt;
			}
			return $tt;
		} else {
			return false;
		}
	}
	
	function fetch_groups(&$user) {
		$possibleGroups = explode("*",$user['tx_itypoexpiringfegroups_groups']);
	 	if (!is_array($possibleGroups)) $possibleGroups = array($possibleGroups);
	 	while (list(,$d) = each($possibleGroups)) {
	 		if (!$d) continue;
		 	list($gr_id,$st_date,$exp_date) = explode("|",$d);
			 	
			$addGroups[$gr_id][] = array(
				"id" => $gr_id,
				"startdate"	=>	$st_date,
				"enddate"	=>	$exp_date,
			);
		}
		return $addGroups;
		
	}

	/**
	 * @param user record
	 * @param group ID
	 * @param startTime or the time period if no Endtime is specified
	 * @param endtime a unixtimestamp
	 * @param ExtendTime ... add the time period to another record extendimg the membership or create a new line
	 *
	 * @return nothing But modifies the original user record
	 */
	function addToGroup(&$user,$group_id, $startTime, $endTime = 0, $extend = true) {
		$groups = $this->fetch_groups(&$user);
		if(($endTime == 0) || ($endTime < $startTime)) {
			 $endTime = time()+$startTime;
			 $startTime = time();
		}
		$duration  =  $endTime -  $startTime;

		if(!$extend || !is_array($groups[$group_id]) ) {
			$groups[$group_id][] = array(
				"id" => $group_id,
				"startdate"	=>	$startTime,
				"enddate"	=>	$endTime,
			);
		} else {
			$len = count($groups[$group_id])-1;
			if ($groups[$group_id][$len]['enddate'] > time()) {
				$groups[$group_id][$len]['enddate'] += $duration;
			} else {
				$groups[$group_id][] = array(
					"id" => $group_id,
					"startdate"	=>	$startTime,
					"enddate"	=>	$endTime,
				);
			 }
		}
		
		$user['tx_itypoexpiringfegroups_groups'] = $this->compactArray($groups);
	}
        
	function compactArray($groups) {
		$parts = array();
		foreach ($groups as $item) {
			foreach ($item as $record) {
				$parts[] = $record['id']."|".$record['startdate']."|".$record['enddate'];
			}
		}

		return implode("*",$parts);
	}
}

?>