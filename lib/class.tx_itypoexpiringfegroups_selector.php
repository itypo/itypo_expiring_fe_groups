<?php

/***************************************************************
*  Copyright notice
*  
*  (c) 2013 Sander Leeuwesteijn | iTypo (info@itypo.nl)
*  Based on extension from: (c) 2004 Radu Cocieru (raducocieru@hotmail.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/** 
 * The TCA field for selecting the groups & dates.
 *
 * @author	Sander Leeuwesteijn | iTypo <info@itypo.nl>
 * @author	Radu Cocieru <raducocieru@hotmail.com>
 */
class tx_itypoexpiringfegroups_selector {
	function main(&$PA,&$pObj){
		global $BE_USER,$LANG,$BACK_PATH,$TCA_DESCR,$TCA,$CLIENT,$TYPO3_CONF_VARS;
		
		$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery("uid, title","fe_groups","deleted=0","","title");
		$groups = "";
		$rawGroups = array();
		if ($res) {
			while ($row =  $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
				$rawGroups[$row['uid']] = $row['title'];
				$groups .= "<option value='".htmlspecialchars($row[uid])."' >".$row['title']."</option>\n\t";
			}
		}
		
		$currentValues = explode('*',$PA['row']['tx_itypoexpiringfegroups_groups']);
		$currentValue = "";
		while (list($index,$val)= each($currentValues)) {
			if ($val == "") continue;
			$parts = explode("|",$val);
			if (!is_array($parts)) continue;
			$currentValue .= "<option value='".htmlspecialchars($val)."'>".$rawGroups[$parts[0]]." (";
			
			if ($parts[1] > 0) {
                if (strftime("%H:%M",$parts[1])!="00:00") {
                    $currentValue .= strftime("%H:%M %d-%m-%Y",$parts[1]);
                } else {
					$currentValue .= strftime("%d-%m-%Y",$parts[1]);
				}
			}
			
			if ($parts[2] > 0) {
                if (strftime("%H:%M",$parts[1])!="00:00") {
                    $currentValue .= " to: ".strftime("%H:%M %d-%m-%Y",$parts[2]);
                } else {
                    $currentValue .= " to: ".strftime("%d-%m-%Y",$parts[2]);
				}
			}

			$currentValue .= ")</option>";	
		}
		
		$field_PA = array(
		    "fieldConf" => Array(
        	    "config" => Array(
                    "type" => "input",
                    "size" => 12,
                    "max" => 20,
                    "eval" => 'datetime',
                    "default" => 0,
                    "checkbox" => 0,
                    "form_type" => "input",
                )
        	),
			"label" => "Start date",
    		"fieldTSConfig" => '',
    		"itemFormElValue" => 0,
    		"onFocus" => '', 
		    "fieldChangeFunc" => Array(
            	"TBE_EDITOR_fieldChanged" => "",
            	"alert" => '', 
        	)			
		);		
		
		$html = "<script>
				function addExpGroup() {
					var rawField = document.forms[0]['".$PA['itemFormElName']."'];
					var groups = document.forms[0]['tx_exp_gr_select'];
					var gr_id = groups.options[groups.selectedIndex].value;
					
					var newText = groups.options[groups.selectedIndex].text;
					var newVal = gr_id+\"|\"+document.forms[0]['tx_exp_gr_st'].value+\"|\"+document.forms[0]['tx_exp_gr_ed'].value;
					rawField.value += newVal+\"*\";
					
					newText += ' ('+document.forms[0]['tx_exp_gr_st_hr'].value+' to:'+document.forms[0]['tx_exp_gr_ed_hr'].value+')';
					
					var gui_selector = document.forms[0]['tx_exp_gr_ui'];
					gui_selector.options[gui_selector.options.length] = new Option(newText,newVal);
				
					TBE_EDITOR_fieldChanged('fe_users','2','tx_itypoexpiringfegroups_groups','".$PA['itemFormElName']."');	
				}
				
	
				
				function removeExpGroup() {				
					var gui_selector = document.forms[0]['tx_exp_gr_ui'];
					var itemToDelete = gui_selector.options[gui_selector.selectedIndex].value;
					gui_selector.options[gui_selector.selectedIndex] = null;
					var rawField = document.forms[0]['".$PA['itemFormElName']."'];
					var parts = rawField.value.split('*');
					var tempStr = \"\";
					for(var i=0;i<parts.length;i++){
						if(parts[i] === \"\") continue;
						if(itemToDelete !== parts[i]){ //continue;
							tempStr += parts[i]+'*';	
						}	
					}
					//alert(tempStr);
				
					rawField.value = tempStr;
					TBE_EDITOR_fieldChanged('fe_users','2','tx_itypoexpiringfegroups_groups','".$PA['itemFormElName']."');					
				}
				</script>";
				
		$selItems = $GLOBALS['LANG']->sL('LLL:EXT:itypo_expiring_fe_groups/locallang_db.xml:fe_users.selected');
		$availItems = $GLOBALS['LANG']->sL('LLL:EXT:itypo_expiring_fe_groups/locallang_db.xml:fe_users.available');
		$gr = $GLOBALS['LANG']->sL('LLL:EXT:itypo_expiring_fe_groups/locallang_db.xml:fe_users.groups');
		$from = $GLOBALS['LANG']->sL('LLL:EXT:itypo_expiring_fe_groups/locallang_db.xml:fe_users.from');
		$until = $GLOBALS['LANG']->sL('LLL:EXT:itypo_expiring_fe_groups/locallang_db.xml:fe_users.until');
		$add = $GLOBALS['LANG']->sL('LLL:EXT:itypo_expiring_fe_groups/locallang_db.xml:fe_users.add');
		
		$html .= "<table cellspacing='5' cellpadding='0'>
					<tr>
						<td>$selItems</td>
						<td></td>
						<td>$availItems</td>
					</tr>
					<tr>
						<td valign='top'><select size='7' name='tx_exp_gr_ui' style='min-width:150px;'>$currentValue</select></td>
						<td valign='top'><input type='image' src='gfx/group_clear.gif' onClick='removeExpGroup(); return false; ' title='Remove' style='padding:0px;font-weight:bold;' /></td>
						<td valign='top'>
							<table>
								<tr>
									<th align='right'>$gr</th>
									<td><select size='1' name='tx_exp_gr_select' >$groups</select></td>
								</tr>
								<tr>
									<th align='right'>$from</th>
									<td>";	
										$field_PA['itemFormElName'] = 'tx_exp_gr_st';
										$field_PA['itemFormElName_file'] = $field_PA['itemFormElName'];		
		$html .= 						$pObj->getSingleField_typeInput("fe_users",'tx_itypoexpiringfegroups_groups',$PA['row'],$field_PA);	
		$html .= "					</td>
								</tr>
								<tr>
									<th align='right'>$until</th>
									<td>";
										$field_PA['itemFormElName'] = 'tx_exp_gr_ed';
										$field_PA['itemFormElName_file'] = $field_PA['itemFormElName'];
										$field_PA['fieldChangeFunc'] = array();
		$html .= 						$pObj->getSingleField_typeInput("fe_users",'tx_itypoexpiringfegroups_groups',$PA['row'],$field_PA);		
		$html .= "					</td>
								</tr>
								<tr>
									<td></td>
									<td><input type='button' value='$add' onClick='addExpGroup();' /></td>
								</tr>
							</table>
							<input type='hidden' value='".htmlspecialchars($PA['itemFormElValue'])."' name='".$PA['itemFormElName']."' />
						</td>
					</tr>
				</table>";
		
		return $html;
	}
}

if (defined("TYPO3_MODE") && $TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/itypo_expiring_fe_groups/class.tx_itypoexpiringfegroups_selector.php"])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/itypo_expiring_fe_groups/class.tx_itypoexpiringfegroups_selector.php"]);
}

?>