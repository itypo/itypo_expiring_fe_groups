<?php

/***************************************************************
*  Copyright notice
*  
*  (c) 2013 Sander Leeuwesteijn | iTypo (info@itypo.nl)
*  Based on extension from: (c) 2004 Radu Cocieru (raducocieru@hotmail.com)
*  All rights reserved
*
*  This script is part of the TYPO3 project. The TYPO3 project is 
*  free software; you can redistribute it and/or modify
*  it under the terms of the GNU General Public License as published by
*  the Free Software Foundation; either version 2 of the License, or
*  (at your option) any later version.
* 
*  The GNU General Public License can be found at
*  http://www.gnu.org/copyleft/gpl.html.
* 
*  This script is distributed in the hope that it will be useful,
*  but WITHOUT ANY WARRANTY; without even the implied warranty of
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*  GNU General Public License for more details.
*
*  This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

/** 
 * Service 'Expiring FE Groups filter' for the 'itypo_expiring_fe_groups' extension.
 *
 * @author	Sander Leeuwesteijn | iTypo <info@itypo.nl>
 * @author	Radu Cocieru <raducocieru@hotmail.com>
 */
class tx_itypoexpiringfegroups_sv1 extends tx_sv_authbase {
	var $prefixId 		= 'tx_itypoexpiringfegroups_sv1';					// Same as class name
	var $scriptRelPath 	= 'sv1/class.tx_itypoexpiringfegroups_sv1.php';		// Path to this script relative to the extension dir.
	var $extKey 		= 'itypo_expiring_fe_groups';						// The extension key.
	
	/**
	 * find usergroups
	 *
	 * @param    array     Data of user.
	 * @param    array     Group data array of already known groups. This is handy if you want select other related groups.
	 *
	 * @return   mixed     groups array
	 */
	 function getGroups($user, $knownGroups){
	    $groupDataArr = array();

		if ($this->mode == 'getGroupsFE') {
		 	$addGroups = array();
		 	$now = time();
 	
		 	$possibleGroups = t3lib_div::trimExplode("*",$user['tx_itypoexpiringfegroups_groups']);
		 	if (!is_array($possibleGroups)) $possibleGroups = array($possibleGroups);
		 	while (list(,$d) = each($possibleGroups)) {
		 		if (!$d) continue;
			 	list($gr_id,$st_date,$exp_date) = t3lib_div::trimExplode("|",$d);
			 	
			 	if ($st_date > 0 && $st_date > $now) continue;
			 	if ($exp_date > 0 && $now > $exp_date) continue; 
				$addGroups[] = $gr_id;			 	
			}
		
			if (count($addGroups)>0) {
				$addGroups = implode(",",$addGroups);
				$where = " deleted = 0 AND hidden = 0 AND uid IN ($addGroups)";
				$res = $GLOBALS['TYPO3_DB']->exec_SELECTquery('*', $this->pObj->usergroup_table, $where);

				if ($res) {
					while ($row = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($res)) {
						$groupDataArr[$row['uid']]=$row;
					}
				
					$GLOBALS['TYPO3_DB']->sql_free_result($res);
				}
			}				
		}
		
		return $groupDataArr;
	}
}

if (defined("TYPO3_MODE") && $TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/itypo_expiring_fe_groups/sv1/class.tx_itypoexpiringfegroups_sv1.php"])	{
	include_once($TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/itypo_expiring_fe_groups/sv1/class.tx_itypoexpiringfegroups_sv1.php"]);
}

?>