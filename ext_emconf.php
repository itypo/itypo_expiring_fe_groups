<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "itypo_expiring_fe_groups".
 *
 * Auto generated 31-01-2013 13:20
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'iTypo Expiring FE Groups',
	'description' => 'Allows to give an fe_group to an fe_user for a specified amount of time.',
	'category' => 'fe',
	'author' => 'Sander Leeuwesteijn | iTypo, Radu Cocieru',
	'author_email' => 'info@itypo.nl',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => 'rc_expiring_fe_groups',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'iTypo',
	'version' => '1.0.3',
	'constraints' => array(
		'depends' => array(
			'typo3' => '4.0.0-4.7.99',
		),
		'conflicts' => array(
			'rc_expiring_fe_groups' => '',
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:10:{s:9:"ChangeLog";s:4:"45b3";s:12:"ext_icon.gif";s:4:"b5e1";s:17:"ext_localconf.php";s:4:"0f6b";s:14:"ext_tables.php";s:4:"64fb";s:14:"ext_tables.sql";s:4:"cbf2";s:16:"locallang_db.xml";s:4:"5cf8";s:14:"doc/manual.sxw";s:4:"f54d";s:42:"lib/class.tx_itypoexpiringfegroups_api.php";s:4:"db40";s:47:"lib/class.tx_itypoexpiringfegroups_selector.php";s:4:"ffa6";s:42:"sv1/class.tx_itypoexpiringfegroups_sv1.php";s:4:"10f1";}',
	'suggests' => array(
	),
);

?>